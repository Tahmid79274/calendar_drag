
import 'package:appointment_drag_and_drop/main.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class ConfigureAppointments extends StatefulWidget {
  DateTime selectedDate;
  String duration;
  CalendarView? calendarView;
  ConfigureAppointments({required this.selectedDate,required this.duration,this.calendarView});

  @override
  State<ConfigureAppointments> createState() => _ConfigureAppointmentsState();
}

List<String> services=['Hotstone','Shiatsu','Foot'];
String selectedValue='Hotstone';

List<String> workers=['Wein Lee','Harimoto','Ding Ning'];
String selectedValueWorker='Wein Lee';

List<String> intervals=['30 min','45 min','60 min'];
String selectedInterval='30 min';


class _ConfigureAppointmentsState extends State<ConfigureAppointments> {

  List<Color> colors=[Colors.red,Colors.green,Colors.blue,Colors.blueGrey,Colors.teal];



  List<Color> colorList=[Colors.red,Colors.green,Colors.blue];
  Color selectedColor = Colors.red;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Configure Appointment')),
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.white,
          padding: const EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 10,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Select Service',style: TextStyle(fontSize: 20),),
                  DropdownButton(
                    icon: Icon(Icons.arrow_downward_outlined),
                    isExpanded: true,
                      value: selectedValue,
                      onChanged: (String? newValue){
                        setState(() {
                          selectedValue = newValue!;
                          print(selectedValue);
                        });
                      },
                      items: services.map((String service){
                        return DropdownMenuItem(child: Text(service),value: service,);
                      }).toList()
                  ),
                ],
              ),
              SizedBox(height: 10,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Select Employee',style: TextStyle(fontSize: 20),),
                  DropdownButton(
                    icon: Icon(Icons.arrow_downward_outlined),
                    isExpanded: true,
                      value: selectedValueWorker,
                      onChanged: (String? newValue){
                        setState(() {
                          selectedValueWorker = newValue!;
                          print(selectedValueWorker);
                        });
                      },
                      items: workers.map((String worker){
                        return DropdownMenuItem(child: Text(worker),value: worker,);
                      }).toList()
                  ),
                ],
              ),
              SizedBox(height: 10,),
              ElevatedButton(onPressed: (){
                //print(widget.duration);
                //List<String>time=widget.duration.split(' ');
                int durationForService=int.parse(widget.duration);
                //final DateTime today = DateTime.now();
                print(durationForService);
                DateTime startTime = widget.selectedDate;
                setState(() {
                  //getDataSource2(meetings,startTime.hour,durationForService,selectedValue);
                  meetings.add(Meeting(
                      selectedValue+'-'+selectedValueWorker, startTime, startTime.add(Duration(minutes: durationForService)), const Color(0xFF0F8644), false));
                });

                Navigator.of(context, rootNavigator: true).pop();

                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => const MyHomePage(title: 'Calendar',)),
                );
              }, child: Text('Done'))
            ],
          ),
        ),
      ),
    );
  }
}
