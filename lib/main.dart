import 'package:appointment_drag_and_drop/configuration.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}


class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

List<Meeting> meetings = <Meeting>[];

class Meeting {
  Meeting(this.eventName, this.from, this.to, this.background, this.isAllDay);

  String eventName;
  DateTime from;
  DateTime to;
  Color background;
  bool isAllDay;
}

class _MyHomePageState extends State<MyHomePage> {

  List<String> durations=['30','60','90'];
  String selectDuration='30';

  CalendarController _controller = CalendarController();
  String _text = '';

  void selectionChanged(CalendarSelectionDetails details) {
    if (_controller.view == CalendarView.month ||
        _controller.view == CalendarView.timelineMonth) {
      _text = DateFormat('dd, MMMM yyyy').format(details.date!).toString();
    } else {
      _text = DateFormat('hh a').format(details.date!).toString();
    }
    print('Selected Date: ${_text[0]}${_text[1]}${_text[3]}');
    String selectedHour = _text[0]+_text[1];
    bool isAm = _text[3]=='P'?false:true;
    print('Hour: $selectedHour');
  }
  Color purple = Colors.white;

  DateTime selectedDay=DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
                height: 550,
                child: DragTarget<int>(
                  onAccept: (int data){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>ConfigureAppointments(selectedDate: selectedDay, duration: selectDuration,calendarView: _controller.view,)));
                  },
                  builder: (BuildContext context,
                      List<dynamic> accepted,
                      List<dynamic> rejected){
                    return SfCalendar(
                      allowedViews: [CalendarView.day,CalendarView.month],
                      controller: _controller,
                      dataSource: MeetingDataSource(meetings),
                      initialDisplayDate: DateTime.now(),
                      initialSelectedDate: DateTime.now(),
                      //showNavigationArrow: true,
                      //showWeekNumber: true,
                      showCurrentTimeIndicator: true,
                      showDatePickerButton: true,
                      onTap: (calendarTapDetails) {
                        selectedDay=calendarTapDetails.date!;
                        print(selectedDay);
                        print(_controller.view);
                      },
                      selectionDecoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          border: Border.all(color: Colors.blue,width: 2),
                          borderRadius: const BorderRadius.all(Radius.circular(5))
                      ),
                      view: CalendarView.month,
                      monthCellBuilder: (BuildContext buildContext, MonthCellDetails details){
                        //print('Details:${details.date.day}');
                        return Container(color: purple,child: Center(child: Text(details.date.day.toString())),);
                      },
                      monthViewSettings: MonthViewSettings(
                        //agendaStyle: AgendaStyle(backgroundColor: Colors.orange,),
                        //numberOfWeeksInView: 1,
                          showAgenda: true,
                          agendaViewHeight: 250,
                          agendaItemHeight: 50,
                          appointmentDisplayMode: MonthAppointmentDisplayMode.indicator),
                    );
                  },
                  //child: ,
                ),
              ),
          Flexible(
            child: Container(
              color: Colors.white,
              //height: 200,
              width: double.infinity,
            ),
          ),
        ],
      ),
      floatingActionButton: Draggable<int>(
        data: 10,
        childWhenDragging: Container(color: Colors.lightGreenAccent,child: Text('Drop to the desired Slot'),),
        feedback: Container(color:Colors.blue,child: Text(selectDuration)),
        child: DropdownButton(
          focusColor: Colors.amber,
          dropdownColor: Colors.blueGrey,
          value: selectDuration,
          onChanged: (String? value){
            setState(() {
              selectDuration=value!;
            });
          },
          items: durations.map((String duration){
            return DropdownMenuItem(
              child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(duration,style: TextStyle(color: Colors.black,fontSize: 20),),
            ),value: duration,);
          }
          ).toList(),
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

List<Meeting> _getDataSource() {
  List<Meeting> meetings = <Meeting>[];
  final DateTime today = DateTime.now();
  final DateTime startTime =
  DateTime(today.year, today.month, today.day, 9, 0, 0);
  final DateTime endTime = startTime.add(const Duration(hours: 2));
  meetings.add(Meeting(
      'Hotstone', startTime, endTime, const Color(0xFF0F8644), false));
  return meetings;
}


/*
_AppointmentDataSource _getCalendarDataSource() {
  appointments.add(Appointment(
    startTime: DateTime.now(),
    endTime: DateTime.now().add(Duration(minutes: 10)),
    subject: 'Meeting',
    color: Colors.blue,
    startTimeZone: '',
    endTimeZone: '',
  ));
  appointments.add(Appointment(
    startTime: DateTime.now(),
    endTime: DateTime.now().add(Duration(hours:1, minutes: 10)),
    subject: 'Meeting 2',
    color: Colors.green,
    startTimeZone: '',
    endTimeZone: '',
  ));

  return _AppointmentDataSource(appointments);
}

class _AppointmentDataSource extends CalendarDataSource {
  _AppointmentDataSource(List<Appointment> source) {
    appointments = source;
  }
}*/


class MeetingDataSource extends CalendarDataSource {
  MeetingDataSource(List<Meeting> source) {
    appointments = source;
  }

  @override
  DateTime getStartTime(int index) {
    return appointments![index].from;
  }

  @override
  DateTime getEndTime(int index) {
    return appointments![index].to;
  }

  @override
  String getSubject(int index) {
    return appointments![index].eventName;
  }

  @override
  Color getColor(int index) {
    return appointments![index].background;
  }

  @override
  bool isAllDay(int index) {
    return appointments![index].isAllDay;
  }
}